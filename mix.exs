defmodule Yummy.Mixfile do
  use Mix.Project

  def project do
    [
      app: :yummy,
      version: "1.0.0",
      elixir: "~> 1.10",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Yummy.Application, []},
      extra_applications: [:logger, :runtime_tools, :phoenix_ecto, :absinthe, :absinthe_plug]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      # Framework
      {:phoenix, "~> 1.4.16"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_ecto, "~> 4.1"},
      {:phoenix_html, "~> 2.14"},
      {:plug_cowboy, "~> 2.0"},
      {:ecto_sql, "~> 3.4.0"},
      {:postgrex, ">= 0.0.0"},

      # Plugs
      {:cors_plug, "~> 2.0.2"},

      # GraphQL
      {:absinthe, "~> 1.4.16", override: true},
      {:absinthe_plug, "~> 1.4.7"},
      {:absinthe_phoenix, "~> 1.4.4"},
      {:dataloader, "~> 1.0.7"},
      {:kronky, "~> 0.5.0"},

      # Utils
      {:bcrypt_elixir, "~> 2.2.0"},
      {:comeonin, "~> 5.3.1"},
      {:gettext, "~> 0.17.4"},
      {:poison, "~> 4.0.1", override: true},
      {:hackney, "~> 1.15.2", override: true},
      {:secure_random, "~> 0.5"},
      {:sweet_xml, "~> 0.6"},
      {:timex, "~> 3.6"},
      {:distillery, "~> 2.1", runtime: false},

      # Mails
      {:bamboo, "~> 1.4"},

      # Upload
      {:arc, "~> 0.11.0"},
      {:arc_ecto, "~> 0.11.3"},
      {:ex_aws, "~> 2.1.3"},

      # Dev
      {:credo, "~> 1.3.1", only: :dev, runtime: false},

      # Tests
      {:wallaby, github: "keathley/wallaby", runtime: false, only: :test},
      {:ex_machina, "~> 2.4", only: :test},
      {:faker, "~> 0.13", only: :test},

      # Prometheus
      {:prometheus, "~> 4.5", override: true},
      {:prometheus_ex, "~> 3.0.5"},
      {:prometheus_ecto, "~> 1.4.3"},
      {:prometheus_phoenix, "~> 1.3.0"},
      {:prometheus_plugs, "~> 1.1.5"},
      {:prometheus_process_collector, "~> 1.4.5"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
