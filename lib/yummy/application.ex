defmodule Yummy.Application do
  use Application

  alias Yummy.{
    PhoenixInstrumenter,
    PipelineInstrumenter,
    PrometheusExporter,
    RepoInstrumenter
  }

  def start(_type, _args) do
    import Supervisor.Spec

    # Prometheus
    require Prometheus.Registry
    PhoenixInstrumenter.setup()
    PipelineInstrumenter.setup()
    RepoInstrumenter.setup()

    if :os.type() == {:unix, :linux} do
      Prometheus.Registry.register_collector(:prometheus_process_collector)
    end

    PrometheusExporter.setup()

    children = [
      supervisor(Yummy.Repo, []),
      supervisor(YummyWeb.Endpoint, []),
      supervisor(Absinthe.Subscription, [YummyWeb.Endpoint])
    ]

    opts = [strategy: :one_for_one, name: Yummy.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    YummyWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
